import 'package:flutter/material.dart';
import 'package:dev_fest_kolkata/home_screen.dart';

main() {
  runApp(DevFestApp());
}

class DevFestApp extends StatefulWidget {
  @override
  _DevFestAppState createState() => _DevFestAppState();
}

class _DevFestAppState extends State<DevFestApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomeScreen(),
    );
  }
}
