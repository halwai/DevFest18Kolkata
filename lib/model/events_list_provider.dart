import 'package:flutter/material.dart';

class Event {
  final String speakerName;
  final String speakerDescription;

  final String eventName;
  final String eventDescription;
  final String eventDuration;

  final IconData eventIcon;

  Event({
    this.eventIcon = Icons.headset_mic,
    this.speakerName = "Pawan Kumar",
    this.speakerDescription =
        "Cat ipsum dolor sit amet, jump five feet high and sideways when a shadow moves annoy owner until he gives you food say meow repeatedly until belly rubs",
    this.eventName = "Flutter Basic UI",
    this.eventDescription =
        "Cat ipsum dolor sit amet, jump five feet high and sideways when a shadow moves annoy owner until he gives you food say meow repeatedly until belly rubs",
    this.eventDuration = "3:00 AM to 4:00 AM",
  });
}

List<Event> events = List.generate(8, (_) => Event());
