import 'package:flutter/material.dart';
import 'schedule_screen_bloc.dart' as bloc;
import 'slider/vertical_slider_bloc.dart' as sliderBloc;
import 'slider/vertical_slider.dart';
import 'package:dev_fest_kolkata/model/events_list_provider.dart';

class ScheduleScreen extends StatefulWidget {
  const ScheduleScreen({
    Key key,
  }) : super(key: key);

  @override
  _ScheduleScreenState createState() => _ScheduleScreenState();
}

Widget eventCardBuilder(context) {
  return Center(
    child: new Card(
      child: Padding(
        padding: const EdgeInsets.all(38.0),
        child: FlutterLogo(
          size: 250.0,
        ),
      ),
    ),
  );
}

class _ScheduleScreenState extends State<ScheduleScreen> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Material(
        child: Column(
          children: <Widget>[
            Container(
              color: Colors.yellow,
              height: 24.0,
            ),
            Expanded(
              flex: 1,
              child: Stack(
                children: <Widget>[
                  Builder(
                    builder: eventCardBuilder,
                  ),
                  VerticalSlider(sliderOptions: sliderBloc.SliderOptions.left, events: events),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
