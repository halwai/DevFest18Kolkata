import 'dart:async';
import 'package:dev_fest_kolkata/model/events_list_provider.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

double _selectedItem;

BehaviorSubject<double> _selectedItemController = BehaviorSubject<double>();
Stream<double> get selectedItem => _selectedItemController.stream;
set setSelectedItem(double i) {
  _selectedItem = i;
  _selectedItemController.sink.add(_selectedItem);
}

enum SliderOptions { right, left }

panDown(DragDownDetails dragDetails, BuildContext context) {
  num maxHeight = MediaQuery.of(context).size.height;
  setSelectedItem = dragDetails.globalPosition.dy / maxHeight;
}

panStart(DragStartDetails dragDetails, BuildContext context) {
  num maxHeight = MediaQuery.of(context).size.height;
  setSelectedItem = dragDetails.globalPosition.dy / maxHeight;
}

panUpdate(DragUpdateDetails dragDetails, BuildContext context) {
  num maxHeight = MediaQuery.of(context).size.height;
  setSelectedItem = dragDetails.globalPosition.dy / maxHeight;
}

panCancel() {}
panEnd(DragEndDetails dragEndDetails, BuildContext context) {
  setSelectedItem = 0.0;
}
