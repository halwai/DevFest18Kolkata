import 'package:dev_fest_kolkata/model/events_list_provider.dart';
import 'package:flutter/material.dart';
import 'vertical_slider_bloc.dart' as bloc;

class VerticalSlider extends StatefulWidget {
  final bloc.SliderOptions sliderOptions;
  final List<Event> events;

  const VerticalSlider(
      {Key key, @required this.sliderOptions, @required this.events})
      : super(key: key);

  @override
  VerticalSliderState createState() {
    return new VerticalSliderState();
  }
}

class VerticalSliderState extends State<VerticalSlider> {
  int pieces;

  @override
  void initState() {
    pieces = widget.events.length;
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Row(
        children: <Widget>[
          GestureDetector(
            onPanDown: (_) {
              bloc.panDown(_, context);
            },
            onPanStart: (_) {
              bloc.panStart(_, context);
            },
            onPanUpdate: (_) {
              bloc.panUpdate(_, context);
            },
            onPanCancel: () {
              bloc.panCancel();
            },
            onPanEnd: (_) {
              bloc.panEnd(_, context);
            },
            child: Container(
              color: Colors.transparent,
              child: Center(
                child: SizedBox(
                  width: 50.0,
                ),
              ),
            ),
          ),
          Center(
            child: StreamBuilder(
                stream: bloc.selectedItem,
                initialData: 0,
                builder: (context, snapshot) => Align(
                    alignment: Alignment(0.0, -1 + 2 * snapshot.data),
                    child: Card(
                      child: Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Text(
                                    '${(snapshot.data * pieces).round() ?? 'no data'}'),
                                Text('10:00AM to 11:00PM'),
                              ],
                            ),
                            Icon(Icons.flight),
                          ],
                        ),
                      ),
                    ))),
          ),
        ],
      ),
    );
  }
}
